#ifndef _WRAPPER_H_
#define _WRAPPER_H_

using namespace System;
using namespace System::Runtime::InteropServices;

namespace ManagedDDSTools_Native
{
	public ref class DDSNative
	{
	public:
		enum class Compression
		{
			DXT1,
			DXT3,
			DXT5
		};
		static bool CompressTexture(IntPtr input, int width, int height, IntPtr output, IntPtr outputSize, Compression compression);
		static bool DecompressTexture(IntPtr input, int inputSize, IntPtr output, int width, int height, Compression compression);
	};
}

#endif // _WRAPPER_H_
