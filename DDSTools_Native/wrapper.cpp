#include "wrapper.h"

#include <nvtt/nvtt.h>
#include <vector>

namespace ManagedDDSTools_Native
{
	void updateProgress(int64_t progress)
	{
		// Send Progress Update to C# via events
	}

	bool DDSNative::CompressTexture(IntPtr input, int width, int height, IntPtr output, IntPtr outputSize, Compression compression)
	{
		Byte* in = reinterpret_cast<Byte*>(input.ToPointer());
		Byte* out = reinterpret_cast<Byte*>(output.ToPointer());
		int* outSize = reinterpret_cast<int*>(outputSize.ToPointer());

		int sz = *outSize;

		Byte* data = nvtt::CompressTexture(in, width, height, outSize, (nvtt::DXTCompression)compression, updateProgress);
		if (data == NULL)
			return false;

		memcpy_s(out, *outSize, data, *outSize);

		return true;
	}

	bool DDSNative::DecompressTexture(IntPtr input, int inputSize, IntPtr output, int width, int height, Compression compression)
	{
		Byte* in = reinterpret_cast<Byte*>(input.ToPointer());
		Byte* out = reinterpret_cast<Byte*>(output.ToPointer());
		return nvtt::DecompressTexture(in, inputSize, out, width, height, (nvtt::DXTCompression)compression);
	}
}